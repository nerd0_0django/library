PROJECT_NAME := "library"
PKG := "https://gitlab.com/nerd0_0.library/library.backend"
PKG_LIST := $(pip freeze)
GO_FILES := $(shell find . -name '*.py' | grep -v /venv/)
all: build

build: dep
	python app.py build

dep:
	pip-compile requirements.in > requirements.txt && cat requirements.txt && pip install -r requirements.txt

lint: dep
	flake8