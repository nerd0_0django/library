from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from web.models import Work, Book, Author, Publisher, Collection


@login_required
def index(request):
    return redirect('/works')


class WorksView(TemplateView):
    @method_decorator(login_required)
    def get(self, *args, **kwargs):
        works = list(Work.objects.all())
        return render(self.request, 'works.html', {'works': works})


class BooksView(TemplateView):
    @method_decorator(login_required)
    def get(self, *args, **kwargs):
        books = list(Book.objects.all())
        return render(self.request, 'books.html', {'books': books})


class AuthorsView(TemplateView):
    @method_decorator(login_required)
    def get(self, *args, **kwargs):
        authors = list(Author.objects.all())
        return render(self.request, 'authors.html', {'authors': authors})


class PublishersView(TemplateView):
    @method_decorator(login_required)
    def get(self, *args, **kwargs):
        publishers = list(Publisher.objects.all())
        return render(self.request, 'publishers.html', {'publishers': publishers})


class CollectionsView(TemplateView):
    @method_decorator(login_required)
    def get(self, *args, **kwargs):
        collections = list(Collection.objects.all())
        return render(self.request, 'collections.html', {'collections': collections})
