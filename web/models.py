from django.db import models


class Author(models.Model):
    __tablename__ = "authors"

    # id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=256, unique=True, null=False, blank=False)
    comment = models.CharField(max_length=1024, null=True, blank=True)

    class Meta:
        db_table = 'authors'
        verbose_name = 'Автор'

    def __str__(self):
        return self.name

    # def __init__(self, name, comment=None):
    #     self.name = name
    #     self.comment = "" if comment is None else comment


class Publisher(models.Model):
    __tablename__ = "publishers"

    # id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=256, unique=True, null=False, blank=False)
    comment = models.CharField(max_length=1024, null=True, blank=True)

    class Meta:
        db_table = 'publishers'
        verbose_name = 'Издательство'

    def __str__(self):
        return self.name

    # def __init__(self, name, comment=None):
    #     self.name = name
    #     self.comment = "" if comment is None else comment


class Work(models.Model):
    __tablename__ = 'works'

    # id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=256, unique=True, null=False, blank=False)
    authors = models.ManyToManyField(Author, related_name="authors")
    inside_period = models.CharField(max_length=128, null=True, blank=True)
    e_link = models.CharField(max_length=256, null=True, blank=True)
    comment = models.CharField(max_length=1024, null=True, blank=True)

    class Meta:
        db_table = 'works'
        verbose_name = 'Произведение'

    def __str__(self):
        return self.name

    # def __init__(self, name, author_id, inside_period=None, e_link=None, comment=None):
    #     self.name = name
    #     self.author_id = author_id
    #     self.inside_period = "" if inside_period is None else inside_period
    #     self.e_link = "" if e_link is None else e_link
    #     self.comment = "" if comment is None else comment


class Book(models.Model):
    __tablename__ = "books"

    # id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=256, unique=True, null=False, blank=False)
    publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE)
    publishing_year = models.DateField(null=False, blank=False)
    availability = models.BooleanField(null=False, blank=False, default=True)
    e_link = models.CharField(max_length=256, null=True, blank=True)
    comment = models.CharField(max_length=1024, null=True, blank=True)
    works = models.ManyToManyField(Work)

    class Meta:
        db_table = 'books'
        verbose_name = 'Сборник'

    def __str__(self):
        return self.name

    # def __init__(self, name, publisher_id, publishing_year, availability=True, e_link=None, comment=None):
    #     self.name = name
    #     self.publisher_id = publisher_id
    #     self.publishing_year = publishing_year
    #     self.availability = availability
    #     self.e_link = "" if e_link is None else e_link
    #     self.comment = "" if comment is None else comment


class Collection(models.Model):
    __tablename__ = "collections"

    class Meta:
        db_table = 'collections'
        verbose_name = 'Коллекция'

    # id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=256, unique=True, null=False, blank=False)
    comment = models.CharField(max_length=1024, null=True, blank=True)
    works = models.ManyToManyField(Work)

    def __str__(self):
        return self.name

    # def __init__(self, name, comment=None):
    #     self.name = name
    #     self.comment = "" if comment is None else comment
