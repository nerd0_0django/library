from django.contrib import admin

# Register your models here.
from web.models import Author, Publisher, Work, Book, Collection

admin.site.register(Author)
admin.site.register(Publisher)
admin.site.register(Work)
admin.site.register(Book)
admin.site.register(Collection)
