from django import forms
from django.contrib.auth.forms import AuthenticationForm


class LoginForm(AuthenticationForm):
    username = forms.CharField(max_length=30, required=True, label='Логин',
                               widget=forms.TextInput(attrs={'class': 'fadeIn'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'fadeIn'}), required=True,
                               label='Пароль')
