from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import logout_then_login
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from .forms import LoginForm


class LoginView(TemplateView):
    template_name = "login.html"

    def get(self, request, *args, **kwargs):
        form = LoginForm()
        for field in form:
            print(field)
        return render(request, 'login.html', {'form': form})

    def post(self, request):
        if request.user is not None and request.user.is_active:
            return redirect('index')
        form = LoginForm(request.POST, {
            'username': request.POST.get('username'),
            'password': request.POST.get('password')
        })
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('index')
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid login')
        else:
            return render(request, 'login.html', {'form': form})


@login_required
def logout(request):
    if request.user is not None and request.user.is_active:
        return logout_then_login(request)
    return redirect('/login')
