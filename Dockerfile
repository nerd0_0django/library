FROM python:latest
RUN mkdir -p /app
WORKDIR /app
#RUN apt-get install libmysqlclient-dev
RUN apt update && apt install flake8 -y
RUN pip install pip-tools
RUN pip install -U setuptools
COPY requirements.in .
RUN pip-compile requirements.in > requirements.txt
RUN cat requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
ENV FLASK_APP=run.py
ENV FLASK_ENV=development